package com.amdocs.web.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.amdocs.Calculator;
import com.amdocs.Increment;
import com.amdocs.config.SpringWebConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringWebConfig.class})
@WebAppConfiguration
public class HelloControllerTest {
 
    private MockMvc mockMvc;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
 
    @Test
    public void runHello() throws Exception {

        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                ;
        assertEquals("Result", 9, 9);
    }
    
    @Test
    public void runHelloName() throws Exception {

        mockMvc.perform(get("/hello/Prashant+Beniwal"))
                .andExpect(status().isOk())
                ;
        assertEquals("Result", 9, 9);
    }

    @Test
    public void runNegativeScen() throws Exception {

        mockMvc.perform(get("/try/Prashant+Beniwal"))
                .andExpect(status().isNotFound())
                ;
    }
  @Test
    public void testAdd() throws Exception {

        int result= new Calculator().add();
        assertEquals("Add", 9, result);

    }
   @Test
    public void testSub() throws Exception {

        int result= new Calculator().sub();
        assertEquals("Sub", 3, result);

    }

 
    public void testGetCounter() throws Exception {

        int result= new Increment().getCounter();
        assertEquals("getCounter", 2, result);

    }
 @Test
    public void testDecreaseCounter0() throws Exception {
	
        int result= new Increment().decreasecounter(0);
        assertEquals("decreaseCounter", 0, result);

    }
@Test
    public void testDecreaseCounter1() throws Exception {

        int result= new Increment().decreasecounter(1);
        assertEquals("decreaseCounter", 1, result);

    }
@Test
    public void testDecreaseCounter2() throws Exception {

        int result= new Increment().decreasecounter(2);
        assertEquals("decreaseCounter", 2, result);

    }





}
